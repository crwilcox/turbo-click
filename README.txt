Started On: August 5, 2011

This project was made to help automate mouse clicks. There are plenty of games 
out there that require you to click repeatedly. In the hopes of saving my mouse
(and my wrist), I found a way to automate the procedure.
