using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading; 

namespace TurboClickApplication
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class TurboClick : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
        private System.ComponentModel.Container components = null;
        private Label label1;
        private Button startGrabCoinsButton;
        private Button stopButton;

        static bool GrabCoins=false;
        private Button StartGrowTreeButton;
        static bool GrowTree = false;


		public TurboClick()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.startGrabCoinsButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.StartGrowTreeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Click to Enable Turbo Clicking";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // startGrabCoinsButton
            // 
            this.startGrabCoinsButton.Location = new System.Drawing.Point(12, 48);
            this.startGrabCoinsButton.Name = "startGrabCoinsButton";
            this.startGrabCoinsButton.Size = new System.Drawing.Size(128, 23);
            this.startGrabCoinsButton.TabIndex = 1;
            this.startGrabCoinsButton.Text = "Start Grab Coins";
            this.startGrabCoinsButton.UseVisualStyleBackColor = true;
            this.startGrabCoinsButton.Click += new System.EventHandler(this.startGrabCoinsButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(10, 106);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(130, 23);
            this.stopButton.TabIndex = 2;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // StartGrowTreeButton
            // 
            this.StartGrowTreeButton.Location = new System.Drawing.Point(10, 77);
            this.StartGrowTreeButton.Name = "StartGrowTreeButton";
            this.StartGrowTreeButton.Size = new System.Drawing.Size(128, 23);
            this.StartGrowTreeButton.TabIndex = 3;
            this.StartGrowTreeButton.Text = "Start Grow Tree";
            this.StartGrowTreeButton.UseVisualStyleBackColor = true;
            this.StartGrowTreeButton.Click += new System.EventHandler(this.startGrowTreeButton_Click);
            // 
            // TurboClick
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(384, 262);
            this.Controls.Add(this.StartGrowTreeButton);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.startGrabCoinsButton);
            this.Controls.Add(this.label1);
            this.Name = "TurboClick";
            this.Text = "Turbo Click";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
		#endregion

        [STAThread]
        static void Main() 
        {
            Thread th = new Thread(ButtonPressLoop);
            th.Start();
            Application.Run(new TurboClick());
        }

        private static string startText = "Click Start to Enable Turbo Clicking";

        private void startGrabCoinsButton_Click(object sender, EventArgs e)
        {
            //START
            GrabCoins = true;
            GrowTree = false;
            this.label1.Text = "Click Stop to Disable Grab Coins";
        }

        private void startGrowTreeButton_Click(object sender, EventArgs e)
        {
            //START
            GrowTree = true;
            GrabCoins = false;
            this.label1.Text = "Click Stop to Disable Grow Tree";
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            //STOP
            GrabCoins = false;
            GrowTree = false;
            this.label1.Text = "Click Start to Enable Automation";
        }


        //THREAD
        private static void ButtonPressLoop()
        {
            int startHeight = 150;
            int endHeight = 550;
            int startWidth = 0;
            int endWidth = 800;

            int currentHeight = startHeight;
            int currentWidth = startWidth;

            int spacing = 10;
            int notInUseSpacing = 1000;
            int DELTAWIDTH = 25;
            int DELTAHEIGHT = 50;
            int WAITBETWEENLOOPS = 10 * 1000; //10 seconds
            while(true)
            {
                if (GrabCoins)
                {
                    //store previous position, so that we can send the mouse there after our action
                    Point prev = new Point(Cursor.Position.X, Cursor.Position.Y);

                    var window = WindowScrape.Types.HwndObject.GetWindowByTitle("Plants vs. Zombies");
                    

                    Cursor.Position = new Point(window.Location.X + currentWidth, window.Location.Y + currentHeight);
                    Thread.Sleep(spacing/2);
                    MouseClicker.Mouse.LeftClick();
                    Thread.Sleep(spacing/2);
                    Cursor.Position = prev;
                    
                    
                    currentWidth += DELTAWIDTH;
                    if (currentWidth >= endWidth)
                    {
                        currentWidth = startWidth;
                        currentHeight += DELTAHEIGHT;
                    }
                    if (currentHeight >= endHeight)
                    {
                        currentHeight = startHeight;
                        Thread.Sleep(WAITBETWEENLOOPS);
                    }

                }
                else if (GrowTree)
                {
                    int TIMEBETWEENSTEPS = 250;
                    int CLICKPAUSES = 250;
                    int WAITFORMESSAGEFROMTREE = 3000;

                    int[] SHOPBUTTON = {749, 78};
                    int[] NEXTBUTTON = { 673, 470 };
                    int[] TREEFERTILIZER = {473, 365};
                    int[] YESBUTTON = {312, 415};
                    int[] GOBACK = {429, 576};
                    int[] FERTILIZERGRABBER = {59, 63};
                    int[] BOTTOMOFTREE = { 407, 335};

                    Point prev = new Point(Cursor.Position.X, Cursor.Position.Y);

                    var window = WindowScrape.Types.HwndObject.GetWindowByTitle("Plants vs. Zombies");
                    if (GrowTree)
                    {
                        //Click Shop Button
                        Cursor.Position = new Point(window.Location.X + SHOPBUTTON[0], window.Location.Y + SHOPBUTTON[1]);
                        Thread.Sleep(CLICKPAUSES);
                        MouseClicker.Mouse.LeftClick();
                        Thread.Sleep(TIMEBETWEENSTEPS);
                    }
                    //Attempt to buy 10 bags of fertilizer
                    //finicky, have to 'double tap' this off the bat.  for ease, this is just going to do it 11 times.  
                    //This should get us 10
                    for (int i = 0; i < 11; i++)
                    {
                        if (GrowTree)
                        {
                            //click fertilizer
                            Cursor.Position = new Point(window.Location.X + TREEFERTILIZER[0], window.Location.Y + TREEFERTILIZER[1]);
                            Thread.Sleep(CLICKPAUSES);
                            MouseClicker.Mouse.LeftClick();
                            Thread.Sleep(CLICKPAUSES);
                            //click yes
                            Cursor.Position = new Point(window.Location.X + YESBUTTON[0], window.Location.Y + YESBUTTON[1]);
                            Thread.Sleep(CLICKPAUSES);
                            MouseClicker.Mouse.LeftClick();
                            Thread.Sleep(CLICKPAUSES);
                        }
                    }

                    //now go back to the tree
                    if (GrowTree)
                    {
                        Cursor.Position = new Point(window.Location.X + GOBACK[0], window.Location.Y + GOBACK[1]);
                        Thread.Sleep(CLICKPAUSES);
                        MouseClicker.Mouse.LeftClick();
                        Thread.Sleep(CLICKPAUSES);
                    }

                    //attempt to fertilize tree 10 times
                    for (int i = 0; i < 10; i++)
                    {
                        if (GrowTree)
                        {
                            //grab fertilizer
                            Cursor.Position = new Point(window.Location.X + FERTILIZERGRABBER[0], window.Location.Y + FERTILIZERGRABBER[1]);
                            Thread.Sleep(CLICKPAUSES);
                            MouseClicker.Mouse.LeftClick();
                            Thread.Sleep(CLICKPAUSES);
                            //click bottom of tree
                            Cursor.Position = new Point(window.Location.X + BOTTOMOFTREE[0], window.Location.Y + BOTTOMOFTREE[1]);
                            Thread.Sleep(CLICKPAUSES);
                            MouseClicker.Mouse.LeftClick();
                            Thread.Sleep(WAITFORMESSAGEFROMTREE);
                        }
                    }

                    if (GrowTree)
                    {
                        Thread.Sleep(WAITBETWEENLOOPS);
                    }
                }
                else
                {
                    Thread.Sleep(notInUseSpacing);
                }
            }
        }

	}
}
