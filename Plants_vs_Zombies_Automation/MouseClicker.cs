using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MouseClicker
{
  
		public class Mouse
		{
 
            [DllImport("user32.dll")]
            private static extern void mouse_event(UInt32 dwFlags,UInt32 dx,UInt32 dy,UInt32 dwData,IntPtr dwExtraInfo);

            private enum MouseEvent
            {
                LEFTDOWN = 0x00000002,
                LEFTUP = 0x00000004,
                MIDDLEDOWN = 0x00000020,
                MIDDLEUP = 0x00000040,
                MOVE = 0x00000001,
                ABSOLUTE = 0x00008000,
                RIGHTDOWN = 0x00000008,
                RIGHTUP = 0x00000010
            }
 
            public static void LeftClick()
            {
                mouse_event((int)MouseEvent.LEFTDOWN, 0, 0, 0, new System.IntPtr());
                mouse_event((int)MouseEvent.LEFTUP, 0, 0, 0, new System.IntPtr());
            }


            public static void LeftClick(int x, int y)
            {
                Cursor.Position = new System.Drawing.Point(x, y);
                LeftClick();
            }

		}
}
