using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading; 

namespace TurboClickApplication
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class TurboClick : System.Windows.Forms.Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
        private System.ComponentModel.Container components = null;
        private Label label1;

        static bool MouseClickingEnabled=false;

		public TurboClick()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Click to Enable Turbo Clicking";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TurboClick
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(419, 57);
            this.Controls.Add(this.label1);
            this.Name = "TurboClick";
            this.Text = "Turbo Click";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
		#endregion

        [STAThread]
        static void Main() 
        {
            Thread th = new Thread(ButtonPressLoop);
            th.Start();
            Application.Run(new TurboClick());

        }

        private static string startText = "Click to Enable Turbo Clicking";
        private void Form1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {

            if (MouseClickingEnabled == true) { 
                MouseClickingEnabled = false;
                this.label1.Text = startText;
                return;
            }
            //ELSE
            ActivateTurbo();

        }

        private void ActivateTurbo()
        {
            try
            {
                this.label1.Text = "Enabled in 3";
                ActiveForm.Refresh();
                Thread.Sleep(1000);

                this.label1.Text += "...2";
                ActiveForm.Refresh();
                Thread.Sleep(1000);

                this.label1.Text += "...1";
                ActiveForm.Refresh();
                Thread.Sleep(1000);

                MouseClickingEnabled = true;
                this.label1.Text = "Hover Over to Disable Turbo Clicking";
            }
            catch(NullReferenceException){
               
            }
        }


        //THREAD
        private static void ButtonPressLoop()
        {
            int spacing = 100;
            int notInUseSpacing = spacing * 10;
            while(true)
            {
                if (MouseClickingEnabled)
                {
                    MouseClicker.Mouse.LeftClick();
                    Thread.Sleep(spacing);
                }
                else
                {
                    Thread.Sleep(notInUseSpacing);
                }
            }
        }
	}
}
